#!/bin/bash

cd 1_base

pdflatex -shell-escape tccudesc && \
pdflatex -shell-escape tccudesc && \
bibtex   tccudesc && \
pdflatex tccudesc

cd ..
